<?php

require('animal.php');
require ('Frog.php');
require ('Ape.php');

$sheep = new Animal("shaun");

echo "Nama Animal:  $sheep->name <br>"; // "shaun"
echo "Jumlah Kaki:  $sheep->legs <br>"; // 2
echo "Termasuk dalam Kelompok Binatang Berdarah Dingin:  $sheep->cold_blooded <br>"; // false
echo "<br>";
$kodok = new Frog ("buduk");
echo "Nama Animal: $kodok->name <br>";
echo "Jumlah Kaki: $kodok->legs <br>";
echo "Termasuk dalam Kelompok Binatang Berdarah Dingin: $kodok->cold_blooded <br>";

$kodok->jump() ; // "hop hop"
echo "<br><br>";
$sungokong = new Ape("kera sakti");
echo "Nama Animal: $sungokong->name <br>";
echo "Jumlah Kaki: $sungokong->legs <br>";
echo "Termasuk dalam Kelompok Binatang Berdarah Dingin: $sungokong->cold_blooded <br>";

$sungokong->yell() ;// "Auooo"
